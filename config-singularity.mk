################################################################################
## CONTAINER SETUP
################################################################################

# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# MUST HAVE FOR MODULES AND CONTAINERS TO WORK W/O WARNINGS
SHELL := /bin/bash
export LC_ALL=C

##### Container Images
hisat      := /apps/singularity-3/hisat2/hisat2-v2.1.0-2-deb_cv1.sif
braker     := /apps/singularity-3/braker2/braker2-2.1.5--1.sif
trinity    := $(pipeline)/trinity_2.11.0--h5ef6573_0.sif
blast      := /apps/singularity-3/blast/blast-2.9.0.sif
mikado     := /apps/singularity-3/mikado/mikado-2.0rc2--py38h6ed170a_1.sif
samtools   := /apps/singularity-3/samtools/samtools-v1.9-4-deb_cv1.sif
#maker      := /apps/singularity-3/maker/maker-2.31.10--pl526h61907ee_17.sif
maker      := $(pipeline)/maker_3.01.03--pl5262h8f1cd36_1.sif
stringtie  := /apps/singularity-3/stringtie/stringtie-2.1.2--h7e0af3c_1.sif
cufflinks  := /apps/singularity-3/cufflinks/cufflinks-2.2.1--py36_2.sif
portcullis := /apps/singularity-3/portcullis/portcullis-1.2.2--py38h8097dd8_1.sif
tetools    := $(pipeline)/tetools_1.5.sif
bedtools   := /apps/singularity-3/bedtools/bedtools-2.28.0.sif
##### Create bind flags for each unique real and absolute directory for
##### singularity
# abspath   = full path to given file
# realpath  = dereferenced path to file (useful for symlinks)
# dir       = cut filename portion of paths
# sort      = returns unique directories
# addprefix = add -B to the paths for singularity
container_binds = $(addprefix -B, $(sort $(dir $(realpath $(1)) $(abspath $(1)))))

load_singularity := ml singularity/3.7.1

## Convenience variable for executing container tools
singularity := $(load_singularity) && \
		singularity exec --home $(pipeline)/home/  $(addprefix -B, $(sort $(realpath .) $(abspath .)))

## Call function for singularity + binds
## EX: $(call sexec,$^,$(samtools)) samtools view
sexec = $(singularity) $(call container_binds, $1) $2
spipe = singularity exec $(call container_binds, $1) $2
