# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(pipeline)/config.mk

#########################################
####### Required Input Parameters #######
#########################################
#                    NAME    Description
# $(call var_required, GENOME, Genome Fasta)

##### Changes from MAKER Defaults
MODEL_ORG ?= "\#"
REPEAT_PROTEIN ?= "\#"
ALT_SPLICE ?= 1
ALWAYS_COMPLETE ?= 1
CLEAN_TRY ?= 1
CLEAN_UP ?= 1
MAX_DNA_LEN ?= 999999999
AUGUSTUS_SPECIES := $(notdir $(AUG_SPECIES))


##### List of parameter variables for control files
PARAM_LIST:=MODEL_ORG \
			REPEAT_PROTEIN \
			ALT_SPLICE \
			ALWAYS_COMPLETE \
			CLEAN_TRY \
			CLEAN_UP \
			MAX_DNA_LEN \
			AUGUSTUS_SPECIES \

##### List of file variables for control files
FILE_LIST:=	GENOME \
			EST EST_GFF \
			ALTEST ALTEST_GFF \
			PROTEIN PROTEIN_GFF \
			RM_GFF RMLIB \
			SNAPHMM \
			GMHMM \
			PRED_GFF

.PHONY: all
all: maker-run-all


################################################################################
## 2) Train Augustus
.PHONY: autoaug
################################################################################
augustus.trained: $(GENOME) $(ANNO) | species/
	$(load_singularity) && \
	export AUGUSTUS_CONFIG_PATH="/usr/local/config/" && \
	singularity exec \
		--no-home \
		-B species/:/usr/local/config/species/autoaug \
		-B $(pipline) \
		$(call container_bind,$^) \
		$(maker)
	$(pipeline)/Augustus/scripts/autoAug.pl --species=autoaug \
					--genome=$(word 1,$^) \
					--trainingset=$(word 2,$^) \
					-v -v -v --useexisting \
					--cpus=$(CPUS) \
					--singleCPU
	touch $@


################################################################################
## 3) Create Control Files
.PHONY: maker-setup
################################################################################
maker-setup: maker_opts.ctl maker_exe.ctl maker_bopts.ctl

##### Create opts file
# If any variable in PARAM_LIST is defined add the option
# If any variable in FILE_LIST is defined add abspath as option
maker_opts.ctl: $(foreach param,$(FILE_LIST),$($(param)))
	$(singularity) $(maker) maker -OPTS && \
		mv maker_opts.ctl maker_opts.tmp
	$(pipeline)/maker.setup < maker_opts.tmp > $@.tmp \
		$(foreach param,$(PARAM_LIST),\
			$(if $($(param)),--$(call lc,$(param))=$($(param)),)) \
		$(foreach param,$(FILE_LIST),\
			$(if $($(param)),--$(call lc,$(param))=$(abspath $($(param))),))
	rm maker_opts.tmp
	mv $@.tmp $@

##### Create exe file
maker_exe.ctl:
	$(singularity) $(maker) maker -EXE && \
		mv maker_exe.ctl maker_exe.tmp
	$(pipeline)/maker.setup < maker_exe.tmp > $@.tmp \
		--probuild="$(abspath $(GENEMARK)/probuild)" \
		--gmhmme3="$(abspath $(GENEMARK)/gmhmme3)"
	rm maker_exe.tmp
	mv $@.tmp $@

##### Create bopts file
maker_bopts.ctl:
	$(singularity) $(maker) maker -BOPTS

################################################################################
## 5) Run Maker
# ARRAY TARGETS: maker-run-#
################################################################################
MAKER_COMPLETE := $(notdir $(GENOMES:fasta=complete))
$(call job-array-targets,maker,$(MAKER_COMPLETE))

##### Add genome file to MAKER_COMPLETE targets safely
# __setup-maker-targets - creates a target:prereq + \n
# setup-maker-targets - eval __setup-mker-targets
# pairmap - calls setup-maker-targets on maker_complete and genomes lists
define __setup-maker-targets
$(1):$(2)
endef
setup-maker-targets=$(eval $(call __setup-maker-targets,$1,$2))
$(call pairmap, setup-maker-targets, $(MAKER_COMPLETE), $(GENOMES))


##### Run maker
#    $(foreach param,$(FILE_LIST),$($(param))
#         Include all the files it needs so that the container_binds call
#         functions correctly.
#    $(if $(AUG_SPECIES),-B$(AUG_SPECIES):$(CONF_DIR)/$(notdir $(AUG_SPECIES)),)
#         Setup special bind for augustus species if AUG_SPECIES is set
#    --genome $(lastword $^)
#         The extra target prereq is added to the end of $^, so $(lastword $^)
#         is genome file
#    [ grep -eq grep ]
#         Validate all contigs have finished before ouputing .complete file
$(MAKER_COMPLETE): maker_opts.ctl maker_bopts.ctl maker_exe.ctl \
					$(foreach param,$(FILE_LIST),$($(param))) \
					| tmp/
	$(singularity) --cleanenv $(call container_binds,$^ $(GENEMARK))\
		-B $(pipeline)/Base.pm:/usr/local/lib/Bio/Search/Hit/PhatHit/Base.pm \
		$(if $(AUG_SPECIES),-B$(AUG_SPECIES):/usr/local/config/species/$(notdir $(AUG_SPECIES)),) \
	 	$(maker) \
	 	mpirun -n $(call divide,$(CPUS),4) maker -fix_nucleotides \
	 		--genome $(lastword $^)  \
	 		-b $(@:.complete=) \
	 		-c 4 \
	 		-TMP $(abspath $|) \
	 		--ignore_nfs_tmp \
	        $(wordlist 1,3,$^)
	[ \
		$$(grep -c '>' $(lastword $^)) -eq \
		$$(grep -c -e 'FINISHED$$' \
	 		$(@:.complete=).maker.output/$(@:.complete=)_master_datastore_index.log) \
	] || false
	 touch $@

################################################################################
## 6) Create Combined Datastore Index
################################################################################
MAKER_DATASTORES := $(MAKER_COMPLETE:complete=datastore)

## HACK-ish: Since maker repeats the name of the split twice, I had to call the
## datastore file directly instead of setting up the correct dependency
$(MAKER_DATASTORES) : %.datastore : %.complete
	awk '{$$2 = "$*.maker.output/" $$2} 1;' FS="\t" OFS="\t" \
		$*.maker.output/$*_master_datastore_index.log > $@

## Combine splits into single datastore
maker.datastore: $(MAKER_DATASTORES)
	cat $^ > $@

################################################################################
## 7) Create Final Output
.PHONY: maker-run-all maker-run-gff
################################################################################
maker-run-all: all.gff3 all.transcripts.fasta
maker-run-gff: all.gff3
##### Create GFF file
all.gff3: maker.datastore
	$(singularity) $(maker) gff3_merge -s -n -d $< > $@.tmp
	mv $@.tmp $@

##### Create fasta files
# Since both files are create simultaneously, a dummy file is needed
all.proteins.fasta all.transcripts.fasta: fasta.complete
fasta.complete: maker.datastore
	$(singularity) $(maker) fasta_merge -d $<
	rename maker.datastore.all.maker all \
		maker.datastore.all.maker.proteins.fasta \
		maker.datastore.all.maker.transcripts.fasta
	touch $@


