#!/usr/bin/env perl

use strict;
use warnings;
use autodie;

local $\ = "\n";
local $, = "\t";

my $busco_file = shift;
my $gff_file   = shift;

my $current_busco = {};


my $busco_type = {};
my $gene2busco = {};
open(my $busco_fh, $busco_file);
while(<$busco_fh>){
    next if /^#/;
    chomp;

    my ($busco, $status, $gene, undef) = split "\t";

    # Turn duplicated genes into complete for easier processing later
    $status = 'Complete' if($status eq 'Duplicated');
    $busco_type->{$busco} = $status;


    $current_busco->{$busco} = 'Missing';

    unless( $status eq 'Missing') {
        #Remove alignment coordinates from end of gene id
        $gene =~ s/:[0-9]+-[0-9]+$//;

        $gene2busco->{$gene} = $busco
    }
};
close($busco_fh);

my $aed_count = {};
my $aed2busco = {};
open(my $gff_fh, $gff_file);
while(<$gff_fh>){
    next if /^#/;
    chomp;

    $_ = [split "\t", $_, 9];

    # only select mRNAs (only thing ran with busco)
    next unless $_->[2] eq 'mRNA';

    #split attribute column
    my $attr = {map {split '=', $_, 2} split ';', $_->[8]};
    my $id = $attr->{ID};
    my $aed = $attr->{_AED};

    $aed_count->{$aed}++;

    next unless $gene2busco->{$id};

    $aed2busco->{$aed} ||= [];
    push(@{$aed2busco->{$aed}}, $gene2busco->{$id});
};
close($gff_fh);


my $running_score = {'Missing' => scalar keys %$current_busco};
my $running_count = 0;

print "AED", "mRNAs", "Complete", "Duplicated", "Fragmented", "Missing";

#<= 1.00 doesn't print 1.00, I think it's due to floating point encoding
for(my $i = 0.00; $i < 1.01; $i += 0.01){
    my $aed = sprintf("%0.2f", $i);

    # Get all genes with current aed score, protect against undefined aed score
    my $buscos = $aed2busco->{$aed} || [];

    $running_count += $aed_count->{$aed} || 0;

    foreach my $id (@$buscos){

        my $cur = $current_busco->{$id};
        my $type = $busco_type->{$id};

        # skip duplicates, they're only counted once
        next if $cur eq 'Duplicated';

        # set type to duplicate if seen busco before
        $type = 'Duplicated' if($cur eq 'Complete');

        $running_score->{$cur}--;
        $running_score->{$type}++;

        $current_busco->{$id}=$type;
    }

    print $aed, $running_count,
        map {$_||0} @$running_score{qw/Complete Duplicated Fragmented Missing/}
}
