#!/usr/bin/env bash

sb () { sbatch $@ | sed 's/.*Submitted batch job //'; }

declare -A JOB
JOB[prepare]=$(sb --time=30:00 \
                   --cpus-per-task=1 \
                   --ntasks=1 \
                   --job-name prepare \
                   make.sh)

JOB[repeats]=$(sb --time=48:00:00 \
                   --cpus-per-task=48 \
                   --ntasks=1 \
                   --exclusive \
                   --job-name repeats \
                   --dependency=afterok:${JOB[prepare]} \
                   make.sh)

JOB[align_setup]=$(sb --time=2:00:00 \
                   --cpus-per-task=1 \
                   --ntasks=1 \
                   --job-name align-run-setup \
                   --dependency=afterok:${JOB[prepare]} \
                   make.sh)
JOB[align_run]=$(sb --time=12:00:00 \
                 --array=1-6 \
                 --cpus-per-task=12 \
                 --ntasks=1 \
                 --exclusive=user \
                 --mem=64G \
                 --job-name align-run \
                 --dependency=afterok:${JOB[align_setup]} \
                   make.sh)

JOB[braker]=$(sb --time=48:00:00 \
                   --cpus-per-task=48 \
                   --ntasks=1 \
                   --exclusive \
                   --job-name braker \
                   --dependency=afterok:${JOB[repeats]}:${JOB[align_run]} \
                   make.sh)

JOB[mikado_trinity]=$(sb --time=48:00:00 \
                   --cpus-per-task=48 \
                   --ntasks=1 \
                   --exclusive \
                   --job-name mikado-trinity \
                   --dependency=afterok:${JOB[align_run]} \
                   make.sh)
JOB[mikado_not_trinity]=$(sb --time=48:00:00 \
                   --cpus-per-task=4 \
                   --ntasks=12 \
                   --exclusive=user \
                   --job-name mikado-not-trinity \
                   --dependency=afterok:${JOB[align_run]} \
                   make.sh)
JOB[mikado_prepare]=$(sb --time=48:00:00 \
                   --cpus-per-task=48 \
                   --ntasks=1 \
                   --exclusive \
                   --job-name mikado-prepare \
                   --dependency=afterok:${JOB[mikado_trinity]}:${JOB[mikado_not_trinity]} \
                   make.sh)
JOB[mikado_blast]=$(sb --time=12:00:00 \
                 --array=1-30 \
                 --cpus-per-task=12 \
                 --ntasks=1 \
                 --exclusive=user \
                 --mem=64G \
                 --job-name mikado-blast-run \
                 --dependency=afterok:${JOB[mikado_prepare]} \
                   make.sh)
JOB[mikado_subloci]=$(sb --time=12:00:00 \
                 --cpus-per-task=48 \
                 --ntasks=1 \
                 --exclusive=user \
                 --job-name mikado-subloci \
                 --dependency=afterok:${JOB[mikado_blast]} \
                   make.sh)

JOB[blast_altest_setup]=$(sb --time=1:00:00 \
                 --cpus-per-task=1 \
                 --ntasks=1 \
                 --exclusive=user \
                 --job-name maker-blast-altest-setup \
                 --dependency=afterok:${JOB[prepare]} \
                   make.sh)
JOB[blast_altest_run]=$(sb --time=12:00:00 \
                 --array=1-27 \
                 --cpus-per-task=12 \
                 --ntasks=1 \
                 --exclusive=user \
                 --mem=64G \
                 --job-name maker-blast-altest-run \
                 --dependency=afterok:${JOB[blast_altest_setup]} \
                   make.sh)
JOB[blast_altest]=$(sb --time=2:00:00 \
                 --cpus-per-task=1 \
                 --ntasks=1 \
                 --exclusive=user \
                 --job-name maker-blast-altest-run-all \
                 --dependency=afterok:${JOB[blast_altest_run]} \
                   make.sh)

JOB[blast_protein_setup]=$(sb --time=1:00:00 \
                 --cpus-per-task=1 \
                 --ntasks=1 \
                 --exclusive=user \
                 --job-name maker-blast-protein-setup \
                 --dependency=afterok:${JOB[prepare]} \
                   make.sh)
JOB[blast_protein_run]=$(sb --time=12:00:00 \
                 --array=1-27 \
                 --cpus-per-task=12 \
                 --ntasks=1 \
                 --exclusive=user \
                 --mem=64G \
                 --job-name maker-blast-protein-run \
                 --dependency=afterok:${JOB[blast_protein_setup]} \
                   make.sh)
JOB[blast_protein]=$(sb --time=2:00:00 \
                 --cpus-per-task=1 \
                 --ntasks=1 \
                 --exclusive=user \
                 --job-name maker-blast-protein-run-all \
                 --dependency=afterok:${JOB[blast_protein_run]} \
                   make.sh)

JOB[maker_1_setup]=$(sb --time=1:00:00 \
                 --cpus-per-task=1 \
                 --ntasks=1 \
                 --exclusive=user \
                 --job-name maker-iter.1-setup \
                 --dependency=afterok:${JOB[repeats]}${JOB[align_run]}:${JOB[braker]}:${JOB[mikado_subloci]}:${JOB[blast_altest_run]}:${JOB[blast_protein_run]} \
                   make.sh)
JOB[maker_1_run]=$(sb --time=48:00:00 \
                 --array=1-27 \
                 --cpus-per-task=12 \
                 --ntasks=1 \
                 --exclusive=user \
                 --mem=64G \
                 --job-name maker-iter.1-run \
                 --dependency=afterok:${JOB[maker_1_setup]} \
                   make.sh)
