#################################################################################
# Genome Annotation Pipeline
#
# @file
# @version 0.1
#
############ Caveats and Assumptions
#
#
#################################################################################

pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(pipeline)/config.mk

CPUS ?= $(SLURM_CPUS_PER_TASK)

#########################################
####### Required Input Parameters #######
#########################################
#                    NAME      Description
$(call var_required, NAME,     Output Name)
$(call var_required, GENOME,   Input Genome)
$(call var_required, LIBS,     RNA-Seq Input Library Names)
$(call var_required, LINEAGE,  Busco lineage database name )
$(call var_required, CPUS,     Number of threads to use for each task)
$(call var_required, GENEMARK, GeneMark installation directory)
$(call var_required, MIKADO_SCORE, Mikado scoring matrix [mammalian.yaml$(comma) plant.yaml])

MIKADO_SPLIT:=30
MAKER_ITER:=1
GENOME_SPLIT:=20

### Setup variable meant for the maker makefile, removing the prefixed MAKER_
maker-vars := $(foreach var,\
				$(subst MAKER_,,\
					$(filter-out MAKER_PROTEIN MAKER_EST MAKER_ALTEST MAKER_ITER,\
					$(filter MAKER_%, $(.VARIABLES)))),\
				$(var)="$(MAKER_$(var))")


################################################################################
## 0) Prepare Inputs
.PHONY: prepare
################################################################################
prepare: 0-ref/$(NAME).split.mk
##### Copy genome to sub-directory, making sure it's uncompressed
# TODO: Deal with other compressions than gzip
# Mask short regions inside gaps (introduced by bionano)
#
0-ref/$(NAME).genome.fa : $(GENOME) | 0-ref/
	zcat -f $< > $@.tmp
	$(load_singularity) && \
	$(pipeline)/seqtk/seqtk cutN -g $@.tmp \
	| $(call spipe,$@,$(bedtools)) bedtools maskfasta -fi $@.tmp -bed - -fo $@.tmp2
	rm $@.tmp
	mv $@.tmp2 $@

0-ref/$(NAME).split.mk : 0-ref/$(NAME).genome.fa | 0-ref/$(NAME).split/
	perl $(pipeline)/fasta-splitter.pl --prefix $|/genome --number $(GENOME_SPLIT) $<
	ls $|/genome_*.fasta \
	| xargs realpath -e \
	| xargs printf 'GENOME_FASTAS += %s\n' > $@.tmp
	mv $@.tmp $@

-include 0-ref/$(NAME).split.mk

################################################################################
## 1) Repeat Annotation
.PHONY: repeats
################################################################################
repeats: 1-repeats/$(NAME).rm.gff3 1-repeats/$(NAME).genome.fa.masked

##### Run RepeatModeler and RepeatMasker
#    $(MAKE)
#         Automatic variable for make executable. Causes make to do some special
#         things with certain flags (e.g -n and -j flags)
#    $(MFLAGS)
#         Automatic variable for command-line flags passed to make
1-repeats/$(NAME)/.complete : 0-ref/$(NAME).genome.fa | 1-repeats/$(NAME)/
	$(MAKE) $(MFLAGS) -C $| -f $(pipeline)/1-repeats.mk NAME=$(NAME) GENOME=$(abspath $<) all
	touch $@

##### Copy results to high-level directory
1-repeats/$(NAME).rm.gff3 1-repeats/$(NAME).genome.fa.masked : 1-repeats/$(NAME)/.complete
	cp 1-repeats/$(NAME)/$(notdir $@) $@.tmp
	mv $@.tmp $@

################################################################################
## 2) RNA-seq Alignment
.PHONY: align align-run-setup align-run
# Job Array Targets      : align-run-#
################################################################################
align: $(ALIGNMENTS)
align-run-setup: 2-align/$(NAME)/db
align-run: $(ALIGNMENTS)

ALIGNMENTS := $(addprefix 2-align/$(NAME)/, $(addsuffix .bam, $(LIBS)))
$(call job-array-targets,align,$(ALIGNMENTS))

##### Create hisat database
# 2-align/$(NAME)/db is a prefix; so the file 2-align/$(NAME)/db is never
# created. We can use that file as a signal that the database was sucessfully
# made by creating it at the end of the receipe
2-align/$(NAME)/db : 0-ref/$(NAME).genome.fa | 2-align/$(NAME)/
	$(call sexec,$^,$(hisat)) hisat2-build $< $@
	touch $@

##### Align each RNA-seq library to the genome using HISAT2
#  - $*
#         Name of library (given in LIBS variable)
#  - $($*)
#         Read files given on command-line. Name should be in the LIBS list
#  - $$($$*)
#         Secondary expansion of $($*)
#  - $(if $(word 2,$($*)), -1 $(word 1, $($*)) -2 $(word 2, $($*)), -U $($*))
#         If 2nd file exists, then hisat is run with paired data
#         Else hisat is run with single-ended data
#
.SECONDEXPANSION:
$(ALIGNMENTS): 2-align/$(NAME)/%.bam: 2-align/$(NAME)/db \
									   $$(subst $$(comma),$$(space),$$($$*)) \
									| 2-align/$(NAME)/
	$(call sexec,$^,$(hisat)) \
		hisat2 -p $(CPUS) --dta --dta-cufflinks -x $< \
		--summary-file $@.stats \
		$(if $(word 2,$($*)), -1 $(word 1, $($*)) -2 $(word 2, $($*)), -U $($*)) \
	| singularity exec $(samtools) samtools view -bS - \
	| singularity exec $(call container_binds,.) \
           $(samtools) samtools sort -m60G -T $@.temp -o $@.tmp -
	mv $@.tmp $@


################################################################################
## 3) BRAKER
.PHONY: braker
################################################################################
braker: 3-braker/$(NAME)/.complete

##### Copy species directory from container
3-braker/$(NAME)/augustus : | 3-braker/$(NAME)/
	$(call sexec,$^,$(braker)) cp -r /usr/local/config/species/ $@.tmp
	mv $@.tmp $@

##### Run Braker
# -B /bin/nproc
#         bind nproc program so braker can check the number of processors and
#         actually run like it's told. Else it complains that it can't validate
#         the number of processors and reverts to a single thread
# -B $|:/usr/local/config/species
#         binds the species directory to make it writable
#         TODO: try overlays instead. Creating a ramdisk overaly and copying
#         data from container may be a better, more reliable, solution
# -B $(GENEMARK):/genemark
#         Adds the required GeneMark program suite to the container.
# -B $(pipline)/Augustus/scripts/:/usr/local/augustus_scripts/
#         Adds latest versions of the augustus scripts to the container. The
#         scripts in the container are older and don't work with the latest
#         version of genemark
3-braker/$(NAME)/.complete: 1-repeats/$(NAME).genome.fa.masked $(ALIGNMENTS) | 3-braker/$(NAME)/augustus
	$(singularity) -B /bin/nproc -B $|:/usr/local/config/species/ \
				   -B $(GENEMARK):/genemark \
				   -B $(pipeline)/Augustus/scripts/:/usr/local/augustus_scripts/ \
					 $(braker) \
		braker.pl --species=$(NAME) --genome=$< --bam=$(subst $(space),$(comma),$(ALIGNMENTS)) \
				  --softmasking --cores=$(CPUS) --workingdir $(dir $@) \
				  --AUGUSTUS_SCRIPTS_PATH=/usr/local/augustus_scripts \
				  --GENEMARK_PATH=/genemark/ && \
	touch $@

##### Copy the needed results files (gmhmm.mod, species directory, and
##### braker.gtf) from braker's crazy directory structure
3-braker/$(NAME)/gmhmm.mod : 3-braker/$(NAME)/.complete
	cp 3-braker/$(NAME)/GeneMark-ET/gmhmm.mod $@

## TODO: may want to move the nex two targets to maker iter.1 since they aren't
## used in further iterations of maker
3-braker/$(NAME)/species/$(NAME)/.complete : 3-braker/$(NAME)/.complete | 3-braker/$(NAME)/species/
	cp -r 3-braker/$(NAME)/augustus/$(NAME) $|
	touch $@
3-braker/$(NAME)/species/$(NAME): 3-braker/$(NAME)/species/$(NAME)/.complete
3-braker/$(NAME)/braker.gtf : 3-braker/$(NAME)/.complete


################################################################################
## 4) Mikado
.PHONY: mikado
################################################################################
mikado: mikado-all

###### List all named targets in mikado makefile, adding mikado- prefix
MIKADO_RULES :=  $(addprefix mikado-, all not-trinity trinity trinity-align \
			trinity-assemble stringtie cufflinks portcullis setup \
			config prepare blast prodigal serialise subloci)
###### Add blast job array rules
MIKADO_RULES += $(addprefix mikado-blast-run-,$(call sequence, 1,$(MIKADO_SPLIT)))

.PHONY: $(MIKADO_RULES)

###### Run mikado makefile
# Any target starting with mikado- will be passed to the mikado makefile,
# stripped of mikado- (e.g mikado-all = make all)
$(MIKADO_RULES): mikado-% : 0-ref/$(NAME).genome.fa $(ALIGNMENTS) | 4-mikado/$(NAME)/
	$(MAKE) $(MFLAGS) -C $| -f $(pipeline)/4-mikado.mk \
			GENOME=$(abspath $<) \
			ALIGNMENTS="$(abspath $(ALIGNMENTS))" \
			UNIPROT=$(UNIPROT) \
			SCORE=$(MIKADO_SCORE) \
			SPLIT=$(MIKADO_SPLIT) \
			$*

##### Link subloci.gff3 to mikado-all since it's the final target
4-mikado/$(NAME)/subloci.gff3: mikado-all

################################################################################
## 5) Maker Blast
.PHONY: maker-blast
################################################################################

##### Create list of maker blast targets based on input variables
# The following three ifdef statements follow the same format:
# - Check if MAKER_ variable is defined for protein, est, or altest
# - Setup phony target to make all for maker
# - Setup a pattern target to capture all maker-blast- targets for that specific
#   file, preprending maker- to the pattern and passing it to the maker makefile.
#   EX: maker-blast-protein-setup -> maker-setup in protein directory
#       maker-blast-est-run-1     -> maker-run-1 in est directory
#   HACK: Something must be prepended to the pattern so that the
#         job-array-targets function works
#   FIXME: $(MAKE) doesn't trigger the special handling. I'm assuming becuase
#          it's inside a define
MAKER_BLAST_TARGETS :=


define __maker-blast-run-template
MAKER_BLAST_TARGETS += maker-blast-$1
MAKER_$(call uc,$1)_GFF:=5-maker-blast/$(NAME)/$1/all.gff3

.PHONY: maker-blast-$1
maker-blast-$1: maker-blast-$1-setup maker-blast-$1-run-all
maker-blast-$1-run-gff: maker-blast-$1-setup
$(MAKER_$(call uc,$1)_GFF): maker-blast-$1-run-gff

maker-blast-$1-%: 0-ref/$(NAME).genome.fa $(MAKER_$(call uc,$1)) \
						| 5-maker-blast/$(NAME)/$1/
		$(MAKE) $(MFLAGS) -C $$| -f $(pipeline)/5-maker.mk \
		GENOMES="$(GENOME_FASTAS)" $(call uc,$1)=$(abspath $(MAKER_$(call uc,$1))) \
		$(maker-vars) maker-$$*

endef

##### Protein Blast
ifdef MAKER_PROTEIN
$(eval $(call __maker-blast-run-template,protein))
endif

##### EST Blast
ifdef MAKER_EST
$(eval $(call __maker-blast-run-template,est))
endif

##### Alt EST Blast
ifdef MAKER_ALTEST
$(eval $(call __maker-blast-run-template,altest))
endif

maker-blast: $(MAKER_BLAST_TARGETS)
################################################################################
## ?) Maker
.PHONY: maker
################################################################################
maker: maker-iter.$(MAKER_ITER)-all

MAKER_ITERATIONS := $(addprefix maker-iter.,$(call sequence,1,$(MAKER_ITER)))

############### Maker Iter 1 input files ###################
# Since the first iteration of maker is slightly different than the others, a
# few specific targets need to be made

##### Create snap input from braker
# Convert braker gtf to zff since it needs special args
5-maker/$(NAME)/iter.1/snap/all.ann	: 3-braker/$(NAME)/braker.gtf \
									| 5-maker/$(NAME)/iter.1/snap/
	$(load_singularity) && \
	$(pipeline)/gffread-0.12.3 $< 2>$@.log \
    |	sed 's/transcript/mRNA/' \
	|	singularity exec $(call container_binds,.) \
			$(maker) maker2zff -n /dev/stdin
	rm genome.dna
	mv genome.ann $@

5-maker/$(NAME)/iter.1/snap/snap.hmm: 0-ref/$(NAME).genome.fa \
									  5-maker/$(NAME)/iter.1/snap/all.ann \
									| 5-maker/$(NAME)/iter.1/snap/
	$(MAKE) $(MFLAGS) -C $| -f $(pipeline)/5.1-snap.mk \
		GENOME=$(abspath $(word 1,$^)) \
		ANNO=$(abspath $(word 2,$^)) \
		train



##### Copy species directory and signal it's creation
5-maker/$(NAME)/iter.1/augustus.trained	: 3-braker/$(NAME)/species/$(NAME) \
										| 5-maker/$(NAME)/iter.1/
	cp $< > $|/species
	touch $@

############################################################


##### Create depedency lists for each iteration of maker
# TODO: Setup a foreach function to make any number the tagets after iter.1
MAKER_ITER.1 = maker-iter.1-setup maker-iter.1-run-all \
		$(addprefix maker-iter.1-run-,$(call sequence,1,$(words $(GENOME_FASTAS))))
$(MAKER_ITER.1) : 0-ref/$(NAME).genome.fa  \
		1-repeats/$(NAME).rm.gff3 \
		3-braker/$(NAME)/species/$(NAME) \
		3-braker/$(NAME)/gmhmm.mod \
		4-mikado/$(NAME)/subloci.gff3 \
		5-maker/$(NAME)/iter.1/snap/snap.hmm \
		$(MAKER_EST_GFF) $(MAKER_ALTEST_GFF) $(MAKER_PROTEIN_GFF) \
		| 5-maker/$(NAME)/iter.1/


###### Run maker makefile
# Any target starting with maker-iter.#- will be passed to the maker makefile,
# stripped of maker-iter.%-
#    EX: maker-iter.1-all = make -f 5-maker.mk -C iter.1 maker-all
#    EX: maker-iter.1-run-1 = make -f 5-maker.mk -C iter.1 maker-run-1
maker-iter.% :
	$(MAKE)  $(MFLAGS) -C $| -f $(pipeline)/5-maker.mk \
		GENOMES="$(GENOME_FASTAS)" \
		GENOME=$(abspath $(word 1,$^)) \
		RM_GFF=$(abspath $(word 2,$^)) \
		AUG_SPECIES=$(abspath $(word 3,$^)) \
		GMHMM=$(abspath $(word 4,$^)) \
		PRED_GFF=$(abspath $(word 5,$^)) \
		SNAPHMM=$(abspath $(word 6,$^)) \
		GENEMARK=$(abspath $(GENEMARK)) \
		$(if $(MAKER_EST_GFF),EST_GFF="$(abspath $(MAKER_EST_GFF))") \
		$(if $(MAKER_ALTEST_GFF),ALTEST_GFF="$(abspath $(MAKER_ALTEST_GFF))") \
		$(if $(MAKER_PROTEIN_GFF),PROTEIN_GFF="$(abspath $(MAKER_PROTEIN_GFF))") \
		$(maker-vars) \
		maker-$(subst $(space),-,$(call rest,$(subst -,$(space),$*)))


###### Copy final maker final iteration output to 5-maker directory
$(addprefix 5-maker/$(NAME)/, all.gff3 all.transcripts.fasta all.proteins.fasta): \
    5-maker/$(NAME)/%: 5-maker/$(NAME)/iter.$(MAKER_ITER)/%
	ln -s $(realpath $<) $@



$(addprefix 5-maker/$(NAME)/iter.1/, all.gff3 all.transcripts.fasta all.proteins.fasta):\
    maker-iter.1-run-all



################################################################################
## 6) BUSCO
################################################################################
.PHONY: busco busco-all
busco: busco-all

busco-all: 5-maker/$(NAME)/all.transcripts.fasta 5-maker/$(NAME)/all.gff3 | 6-busco/$(NAME)/
	$(MAKE) $(MFLAGS) -C $| -f $(pipeline)/6-busco.mk \
	CPUS=$(CPUS) LINEAGE=$(LINEAGE) TYPE=trans FASTA=$(abspath $<) ANNO=$(abspath $(word 2, $^)) \
	all images


################################################################################
## 7) Filter
################################################################################

.PHONY: filter
filter: 7-filter/$(NAME).gff3 7-filter/$(NAME).transcripts.fa 7-filter/$(NAME).proteins.fa

#-- Fix error in quality_filter script
# If an exon belongs to more than one isform and one of those isoforms are
# filtered, then there's a chance that the filtering script leaves a dangling
# comma at the end of the Parent attribute field.
7-filter/$(NAME).gff3: 5-maker/$(NAME)/all.gff3 | 7-filter/
	$(pipeline)/quality_filter -a $(FILTER) $< | \
		sed 's/,$$/;/' > $@

7-filter/$(NAME).transcripts.fa: 7-filter/$(NAME).gff3 $(GENOME)
	$(pipeline)/gffread-0.12.3 $< \
		-g $(abspath $(GENOME)) \
		-w $@

7-filter/$(NAME).proteins.fa: 7-filter/$(NAME).gff3 $(GENOME)
	$(pipeline)/gffread-0.12.3 $< \
		-g $(abspath $(GENOME)) \
		-y $@

################################################################################
## 8) InterProScan
################################################################################

.PHONY: iprscan iprscan-prepare iprscan-combine
iprscan iprscan-combine: 8-iprscan/$(NAME).tsv

IPRSCAN_SEQ := $(addprefix 8-iprscan/$(NAME)-split/$(NAME)_,\
						$(shell seq -f '%03.0f' 1 $(MIKADO_SPLIT)))
IPRSCAN_FASTA := $(addsuffix .fasta,$(IPRSCAN_SEQ))
IPRSCAN_LOGS := $(addsuffix .log,$(IPRSCAN_SEQ))
IPRSCAN_TSV := $(addsuffix .tsv,$(IPRSCAN_SEQ))

$(call job-array-targets,iprscan,$(IPRSCAN_LOGS))

8-iprscan/$(NAME)-split/complete: 7-filter/$(NAME).proteins.fa | 8-iprscan/$(NAME)-split/
	$(pipeline)/fasta-splitter.pl -p $|/$(NAME) -n $(MIKADO_SPLIT) --round-robin $<
	touch $@

iprscan-prepare: 8-iprscan/$(NAME)-split/complete

$(IPRSCAN_FASTA) : 8-iprscan/$(NAME)-split/complete

$(IPRSCAN_LOGS): %.log : %.fasta
	$(call interproscan,$^) \
		-i $< \
		-f xml,tsv \
		-pathways \
		-iprlookup \
		-goterms \
		-dp  \
		-appl 'Pfam, PRINTS, PANTHER, TIGRFAM, SUPERFAMILY, PIRSF, ProSiteProfiles, ProSitePatterns, SMART' \
		-b $* \
		-cpu $(CPUS) > $@.tmp
	mv $@.tmp $@

$(IPRSCAN_TSV): %.tsv : %.log
8-iprscan/$(NAME).tsv: $(IPRSCAN_TSV)
	cat $^ > $@.tmp
	mv $@.tmp $@
################################################################################
## 9) Uniprot Blast
################################################################################

.PHONY: blast
blast: 9-blast/$(NAME).tsv

9-blast/$(NAME).tsv: 7-filter/$(NAME).proteins.fa $(UNIPROT) | 9-blast/
	$(call sexec,$^,$(blast)) \
	blastp  -query $< \
			-db    $(UNIPROT) \
			-outfmt 6 \
			-out $@.tmp \
			-num_threads $(CPUS)
	mv $@.tmp $@

################################################################################
## 10) RENAME and ADD FUNCTION
################################################################################
.PHONY: rename
rename: 10-rename/$(NAME).complete

.SECONDARY: 10-rename/$(NAME).transcripts.functions.fa \
			10-rename/$(NAME).proteins.functions.fa \
			10-rename/$(NAME).function.gff3 \
			10-rename/$(NAME).func-domain.gff3

### Quickly create make file with contig names
10-rename/split/$(NAME)/contigs.mk: 0-ref/$(NAME).genome.fa.fai | 10-rename/split/$(NAME)/
	awk '{printf "contigs += %s\n", $$1}' $< > $@.tmp
	mv $@.tmp $@

### include contig variable and create filenames for map file
include 10-rename/split/$(NAME)/contigs.mk
contigs.map := $(addprefix 10-rename/split/$(NAME)/, $(addsuffix .map, $(contigs)))
contigs.pre := $(addprefix 10-rename/split/$(NAME)/, $(contigs))

### Split gff file base on contig.
$(contigs.pre) : 10-rename/split/$(NAME)/.complete
10-rename/split/$(NAME)/.complete: 7-filter/$(NAME).gff3 | 10-rename/split/$(NAME)/
	sort -k1,1V -k4,5n $< \
	| awk '!/^#/{ print > "10-rename/split/$(NAME)/" $$1 }'
	touch $@


### Targets for individual contigs
##### NOTE: Included path in pattern rule so $* is contig name. No string
##### manipulation needed
$(contigs.map): 10-rename/split/$(NAME)/%.map: 10-rename/split/$(NAME)/%
	$(call sexec,$^,$(maker)) \
	maker_map_ids --prefix "$(NAME).$*G" \
					--iterate 1 \
					--justify 4 \
					--suffix '-' \
					$< \
	| sed 's/\($(NAME).$*G[0-9]\{4\}\)/\100/' > $@.tmp
	mv $@.tmp $@

### Collect contigs maps
##### Using the prereq faidx since bash can be easily overloaded with the number
##### of scaffolds, producing the error "make[2]: execvp: /bin/bash: Argument
##### list too long"
10-rename/$(NAME).map: 0-ref/$(NAME).genome.fa.fai $(contigs.map) | 10-rename/
	awk '{print "10-rename/split/$(NAME)/" $$1 ".map"}' FS="\t" $< \
	| xargs cat > $@.tmp
	mv $@.tmp $@

### Add uniprot functions to transcript and protein fasta files
#### NOTE: $(addprefix -B ,$^) adds bindings for all prereqs inside container
10-rename/$(NAME).transcripts.functions.fa 10-rename/$(NAME).proteins.functions.fa: \
	10-rename/$(NAME).%.functions.fa:   $(UNIPROT) \
										9-blast/$(NAME).tsv \
										7-filter/$(NAME).%.fa  \
									|	10-rename/
	$(call sexec,$^,$(maker)) maker_functional_fasta $^ > $@.tmp
	mv $@.tmp $@

### Add uniprot functions to gff file
#### NOTE: $(addprefix -B ,$^) adds bindings for all prereqs inside container
10-rename/$(NAME).function.gff3:    $(UNIPROT) \
									9-blast/$(NAME).tsv \
									7-filter/$(NAME).gff3 \
							   |	10-rename/
	$(call sexec,$^,$(maker)) maker_functional_gff $^ > $@.tmp
	mv $@.tmp $@

### Add iprscan data to gff file
10-rename/$(NAME).func-domain.gff3: 10-rename/$(NAME).function.gff3 \
									8-iprscan/$(NAME).tsv \
								  | 10-rename/
	$(call sexec,$^,$(maker)) ipr_update_gff $^ > $@.tmp
	mv $@.tmp $@

### Rename gff ids
#### FIXME: destructive inplace command
10-rename/$(NAME).gff3: 10-rename/$(NAME).map 10-rename/$(NAME).func-domain.gff3
	$(call sexec,$^,$(maker)) map_gff_ids $^
	mv $(word 2,$^) $@

### Rename fasta ids
#### FIXME: destructive inplace command
10-rename/$(NAME).transcripts.fa 10-rename/$(NAME).proteins.fa: \
		10-rename/$(NAME).%.fa: 10-rename/$(NAME).map 10-rename/$(NAME).%.functions.fa
	$(call sexec,$^,$(maker)) map_fasta_ids $^
	mv $(word 2,$^) $@

10-rename/$(NAME).complete:	10-rename/$(NAME).proteins.fa \
							10-rename/$(NAME).transcripts.fa \
							10-rename/$(NAME).gff3

# end

################################################################################
## 99) Create final report for manuscript
################################################################################
.PHONY: final final-busco-transcript

final: 99-final/$(NAME).multi-exon.png \
       99-final/$(NAME).go.level.png \
       99-final/$(NAME).base-summary.tsv \
	   99-final/$(NAME).busco.genome.txt \
       99-final/$(NAME).busco.unfiltered_transcripts.txt \
	   99-final/$(NAME).busco.transcript.txt \
       99-final/$(NAME).proteins.fa \
       99-final/$(NAME).transcripts.fa \
       99-final/$(NAME).gff3


99-final/$(NAME).proteins.fa 99-final/$(NAME).transcripts.fa 99-final/$(NAME).gff3 : \
99-final/% : 10-rename/% | 99-final/
	cp $< $@


99-final/$(NAME).multi-exon.png: 10-rename/$(NAME).gff3 | 99-final/
	ml r/4.0.2 && Rscript $(pipeline)/multi-exon.plot.R $^ $@

99-final/$(NAME).go.level.png: 10-rename/$(NAME).gff3 | 99-final/
	ml r/4.0.2 && Rscript $(pipeline)/go.level.graph.R $^ $@

99-final/$(NAME).base-summary.tsv: 0-ref/$(NAME).genome.fa | 99-final/
	perl $(pipeline)/base-pair-count.pl $< > $@.tmp
	mv $@.tmp $@

99-final/$(NAME).busco.genome.txt: \
        99-final/$(NAME)-genome/busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt
	cp $< $@

99-final/$(NAME).busco.unfiltered_transcripts.txt: \
        6-busco/$(NAME)/busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt
	cp $< $@

99-final/$(NAME).busco.transcript.txt: \
      99-final/$(NAME)-busco-transcripts/busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt
	cp $< $@

final-busco-transcript: \
	99-final/$(NAME)-busco-transcripts/busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt

99-final/$(NAME)-busco-transcripts/busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt : \
		10-rename/$(NAME).transcripts.fa | 99-final/$(NAME)-busco-transcripts/
	$(MAKE) $(MFLAGS) -C $| -f $(pipeline)/6-busco.mk CPUS=$(CPUS)	\
	LINEAGE=$(LINEAGE) TYPE=trans FASTA=$(abspath $<) all

busco-genome: \
	99-final/$(NAME)-genome/busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt | 99-final/$(NAME)-genome/

99-final/$(NAME)-genome/busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt : \
		0-ref/$(NAME).genome.fa $(LINEAGE) | 99-final/$(NAME)-genome/
	$(MAKE) $(MFLAGS) -C $| -f $(pipeline)/6-busco.mk \
	CPUS=$(CPUS) LINEAGE=$(LINEAGE) TYPE=genome FASTA=$(abspath $<) \
	all
