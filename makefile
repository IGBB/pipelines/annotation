#! /usr/bin/env make -f
##
# Genome Annotation Pipeline
#
# @file
# @version 0.1

pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

MIKADO_SPLIT:=30
MAKER_ITER:=1
GENOME_SPLIT:=20

.PHONY: help show-queue show-template queue
help:
	@man $(pipeline)/help.1

# Backward compat
queue: queue-all
requeue-failed: queue-failed
show-queue: queue-show

queue-%:
	$(MAKE) $(MFLAGS) -j1 -f $(pipeline)/queue.mk $*

show-template:
	@cat $(pipeline)/make.template.sh

setup:
	$(MAKE) $(MFLAGS) -C $(pipeline)/ -f $(pipeline)/setup.mk

% :
	$(MAKE) $(MFLAGS) -f $(pipeline)/annotate.mf $@
# end
