# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(pipeline)/config.mk

#########################################
####### Required Input Parameters #######
#########################################
#                    NAME     Description
$(call var_required, NAME,    Output Name)
$(call var_required, GENOME,  Input Genome)
$(call var_required, CPUS,    Number of threads to use for each task)




all: $(NAME).rm.gff3 $(NAME).lai


################################################################################
## 0) Copy Genome
# HACK: RM copies genome to tmp folder to modify but keeps permissions. Since
# anything in annex is read-only, RM fails to open with write permissions, and
# dies. Copy and chmod as workaround.
################################################################################

$(NAME).genome.fa : $(GENOME)
	cp $< $@.tmp
	chmod u+w $@.tmp
	mv $@.tmp $@

################################################################################
## 1) RepeatModeler
################################################################################

$(NAME).db.log: $(NAME).genome.fa
	$(singularity) $(tetools) BuildDatabase -name $(NAME) $< |& tee $@.tmp
	mv $@.tmp $@

# The -debug flag keeps the ltr-retreiver results needed for LAI analysis. Needs
# to be cleaned after LAI is run
$(NAME)-families.fa :  $(NAME).repeatmodeler.log
$(NAME).repeatmodeler.log : $(NAME).db.log
	$(singularity) $(tetools) RepeatModeler \
	  -debug \
	  -database $(NAME) \
	  -LTRStruct \
	  -pa $(CPUS) |& tee $@.tmp
	mv $@.tmp $@


################################################################################
## 2) RepeatMasker
################################################################################

$(NAME).genome.fa.out: $(NAME).genome.fa $(NAME)-families.fa
	$(singularity) $(tetools) RepeatMasker \
	  -lib $(word 2, $^) $(NAME) \
	  -xsmall \
	  -pa $(CPUS) \
	  $<

################################################################################
## 3) Maker compatible GFF file
################################################################################

$(NAME).rm.gff3: $(NAME).genome.fa.out
	$(singularity) $(tetools) rmOutToGFF3.pl $< \
	| grep -v -e "Satellite" -e ")n" -e "-rich" \
	| awk '!/^#/ { $$9 = $$9 ";ID=" c++ } 1;' FS="\t" OFS="\t" > $@.tmp
	mv $@.tmp $@


################################################################################
## 4) LAI
################################################################################
$(NAME).pass.list: $(NAME).repeatmodeler.log
	sed -n 's/.*LtrRetriever : tmpdir = //p' $< \
	| xargs -i cp {}/seq.fa.pass.list $@.tmp
	mv $@.tmp $@

$(NAME).lai: $(NAME).genome.fa $(NAME).pass.list $(NAME).genome.fa.out
	$(call sexec,$^,$(tetools)) /opt/LTR_retriever/LAI \
	  -genome $< -intact $(word 2,$^) \
	  -all $(word 3,$^) \
	  -t $(CPUS) -blast /opt/rmblast/bin/
#
