# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(pipeline)/config.mk

#########################################
####### Required Input Parameters #######
#########################################
#                    NAME    Description
$(call var_required, GENOME, Input Genome)
$(call var_required, ANNO,   Input GFF3 Annotaion)

.PHONY: train stats
train: stats snap.hmm
stats: gene-stats.log validate.log


### Create Input Files
all.ann : $(ANNO)
	$(load_singularity) && \
	cat $< | singularity exec $(call container_binds,.) \
				maker2zff -c 0 -e 0 -o 0 -x 0.5 -l 50 /dev/stdin
	mv genome.ann $@


all.dna: $(GENOME) all.ann
	$(load_singularity) && \
	sed -n 's/>//p' $(word 2, $^)  | \
		xargs -n1000 singularity exec $(call container_binds,$^) $(samtools) \
				samtools faidx $< > $@.tmp
	mv $@.tmp $@



gene-stats.log: all.ann all.dna
	$(call sexec,$^,$(maker)) fathom $^  -gene-stats &> $@.tmp
	mv $@.tmp $@

validate.log: all.ann all.dna
	$(call sexec,$^,$(maker)) fathom $^ -validate &> $@.tmp
	mv $@.tmp $@

cat.ann cat.dna: all.ann all.dna
	$(call sexec,$^,$(maker)) fathom $^ -categorize 1000 &> categorize.log
	mv uni.ann cat.ann && mv uni.dna cat.dna

params/export.ann params/export.dna: cat.ann cat.dna | params/
	$(call sexec,$^,$(maker)) fathom $^ -export 1000 -plus &> cat-plus.log
	mv export.ann export.dna params/

snap.hmm: params/export.ann params/export.dna
	cd params/ && \
	$(call sexec,$^,$(maker)) forge export.ann export.dna > ../forge.log
	$(call sexec,$^,$(maker)) hmm-assembler.pl snap params > $@.tmp
	mv $@.tmp $@
