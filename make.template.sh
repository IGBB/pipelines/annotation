#!/usr/bin/env bash

DIR=`pwd`

TASK=$SLURM_JOB_NAME
if [ -n "${SLURM_ARRAY_TASK_ID}" ]; then
    TASK+="-$SLURM_ARRAY_TASK_ID";
fi
make -f $DIR/pipeline/annotate.mf \\
     -C $DIR/annotations/ \\
     UNIPROT=$DIR/uniprot/uniprot_sprot.fasta \\
     -j $SLURM_NTASKS \\
     NAME=B713 \\
     PROTEIN=$DIR/evidence/Gohir.Gorai.uniport.fa \\
     ALTEST=$DIR/evidence/gossypium.ests.fa \\
     GENEMARK=$DIR/pipeline/gmes_linux_64 \\
     MIKADO_SCORE=plant.yaml \\
     MAKER_SPLIT=27 \\
     GENOME=$DIR/genomes/B713/B713.fasta \\
     LIBS='SRR8878623 SRR8878758 SRR8878501 SRR8878554 SRR8878592 SRR8878771' \\
     SRR8878623=$DIR/genomes/transcriptomes/SRR8878623.fastq.gz \\
     SRR8878758=$DIR/genomes/transcriptomes/SRR8878758.fastq.gz \\
     SRR8878501=$DIR/genomes/transcriptomes/SRR8878501.fastq.gz \\
     SRR8878554=$DIR/genomes/transcriptomes/SRR8878554.fastq.gz \\
     SRR8878592=$DIR/genomes/transcriptomes/SRR8878592.fastq.gz \\
     SRR8878771=$DIR/genomes/transcriptomes/SRR8878771.fastq.gz \\
     LINEAGE=none CPUS=$SLURM_CPUS_PER_TASK $TASK
