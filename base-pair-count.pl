#!/usr/bin/env perl

use strict;
use warnings;
use autodie;

use List::Util qw/sum/;

local $, = "\t";
local $\ = "\n";

my $file = shift;
open(my $fh, $file);

my $data = {};
my $chars = {};
while(<$fh>){
    next until /^>(\S+)/;

    my $header = $1;

    $data->{$header} ||= {};
    for(my $tell = tell($fh); $_ = readline($fh); $tell = tell($fh)){
        if(/^>/){
            seek($fh, $tell, 0);
            last;
        }
        chomp;

        foreach my $char (split //){
            $chars->{$char} = 1;
            $data->{$header}->{$char}++;
        }
    }
}

$chars=[sort keys %$chars];
my $all = {};
print 'Contig', 'Length', @$chars;
foreach my $chr (sort keys %$data){
    my $counts = $data->{$chr};
    print $chr, sum(values %$counts), @$counts{@$chars};

    $all->{$_} += $counts->{$_} foreach (@$chars);
}

print "Total", sum(values %$all), @$all{@$chars};
