#################################################################################
# Genome Annotation Pipeline (Queuing)
#
# @file
# @version 0.1
#
############ Caveats and Assumptions
#
#	1) Using the ar tool as a database. Can't use with parallel make (-j)
#
#   2) Using pregenerated rule for ar, so assuming everything needs to look like
#	an object file (i.e *.o). ASSUMPTION NOT TESTED
#
#	3) Everything is queued. Job exits 'ok' immediately if already completed
#
#################################################################################
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

#FIXME: Gives odd error. Not sure why
#include $(pipeline)/gmsl/gmsl

empty:=
space:=$(empty) $(empty)

# queue id database
queued:= $(NAME).queue.a


# queuing command
queue_cmd := sbatch --exclusive=user --kill-on-invalid=no --parsable --hold
task_cpu_time =  --ntasks=$1 --cpus-per-task=$2 --time=$3

create_queue_resources = \
					$(if $($1-nodes), --nodes=$($1-nodes)) \
					$(if $($1-tasks), --ntasks=$($1-tasks)) \
					$(if $($1-cpus),  --cpus-per-task=$($1-cpus)) \
					$(if $($1-time),  --time=$($1-time)) \
					$(if $($1-mem),  --mem=$($1-mem)) \
					$(if $($1-partition), --partition=$($1-partition)) \
					$(if $($1-array),     --array=$($1-array))

# ifneq ("$(empty)", "$(findstring atlas, $(call lc,$(shell hostname)))")
# 	max-cpus := 48
# 	max-time := 72:00:00
# else
	max-cpus := 48
	max-time := 150:00:00
#endif

#### Prepare
sections := prepare.o
prepare-resources-tasks := 1
prepare-resources-cpus  := 1
prepare-resources-time  := 30:00
prepare.o:

#### Repeats
sections += repeats.o
repeats-resources-tasks := 1
repeats-resources-cpus  := $(max-cpus)
repeats-resources-time  := $(max-time)
repeats.o: $(queued)(prepare.o)

#### Alignments
sections += align-run-setup.o \
			align-run.o

align-run-setup-resources-tasks := 1
align-run-setup-resources-cpus  := 1
align-run-setup-resources-time  := 2:00:00
align-run-setup.o: $(queued)(prepare.o)

align-run-resources-tasks := 1
align-run-resources-cpus  := 12
align-run-resources-time  := 12:00:00
align-run-resources-mem   := 95G
align-run-resources-array := 1-$(words $(LIBS))
align-run.o: $(queued)(align-run-setup.o)

#### Braker
sections += braker.o

braker-resources-tasks := 1
braker-resources-cpus  := $(max-cpus)
braker-resources-time  := $(max-time)
braker.o: $(queued)(repeats.o align-run.o)

#### Mikado
sections += mikado-trinity.o	\
			mikado-not-trinity.o\
			mikado-prepare.o	\
			mikado-blast-run.o 	\
			mikado-subloci.o

mikado-trinity-resources-tasks := 1
mikado-trinity-resources-cpus  := $(max-cpus)
mikado-trinity-resources-time  := $(max-time)
mikado-trinity.o: $(queued)(align-run.o)

mikado-not-trinity-resources-tasks := 12
mikado-not-trinity-resources-cpus  := 4
mikado-not-trinity-resources-time  := $(max-time)
mikado-not-trinity.o: $(queued)(align-run.o)

mikado-prepare-resources-tasks := 1
mikado-prepare-resources-cpus  := $(max-cpus)
mikado-prepare-resources-time  := $(max-time)
mikado-prepare.o: $(queued)(mikado-trinity.o mikado-not-trinity.o)

mikado-blast-run-resources-tasks := 1
mikado-blast-run-resources-cpus  := 12
mikado-blast-run-resources-time  := 12:00:00
mikado-blast-run-resources-array := 1-$(MIKADO_SPLIT)
mikado-blast-run-resources-mem   := 95G
mikado-blast-run.o: $(queued)(mikado-prepare.o)

mikado-subloci-resources-tasks := 1
mikado-subloci-resources-cpus  := $(max-cpus)
mikado-subloci-resources-time  := 12:00:00
mikado-subloci.o : $(queued)(mikado-blast-run.o)


maker-deps := repeats.o align-run.o braker.o mikado-subloci.o
##### Maker Blasts
define __maker-blast-template
sections += maker-blast-$1-setup.o	\
			maker-blast-$1-run.o	\
			maker-blast-$1-run-gff.o

maker-blast-$1-setup-resources-tasks := 1
maker-blast-$1-setup-resources-cpus  := 1
maker-blast-$1-setup-resources-time  := 1:00:00
maker-blast-$1-setup.o : $(queued)(prepare.o)

maker-blast-$1-run-resources-tasks := 1
maker-blast-$1-run-resources-cpus  := $(max-cpus)
maker-blast-$1-run-resources-time  := $(max-time)
maker-blast-$1-run-resources-array := 1-$(GENOME_SPLIT)
maker-blast-$1-run.o : $(queued)(maker-blast-$1-setup.o)

maker-blast-$1-run-gff-resources-tasks := 1
maker-blast-$1-run-gff-resources-cpus  := 1
maker-blast-$1-run-gff-resources-time  := 2:00:00
maker-blast-$1-run-gff.o: $(queued)(maker-blast-$1-run.o)

maker-deps += maker-blast-$1-run-gff.o

endef

######## Maker ESTs
ifdef MAKER_EST
$(eval $(call __maker-blast-template,est))
endif

######## Maker Alternative ESTs
ifdef MAKER_ALTEST
$(eval $(call __maker-blast-template,altest))
endif

######## Maker Proteins
ifdef MAKER_PROTEIN
$(eval $(call __maker-blast-template,protein))
endif

#### Maker Iteration 1
sections += maker-iter.1-setup.o 	\
	maker-iter.1-run.o		\
	maker-iter.1-run-all.o

maker-iter.1-setup-resources-tasks := 1
maker-iter.1-setup-resources-cpus  := 1
maker-iter.1-setup-resources-time  := 1:00:00
maker-iter.1-setup.o: $(queued)($(maker-deps))

maker-iter.1-run-resources-tasks := 1
maker-iter.1-run-resources-cpus  := $(max-cpus)
maker-iter.1-run-resources-time  := $(max-time)
maker-iter.1-run-resources-array := 1-$(GENOME_SPLIT)
maker-iter.1-run.o: $(queued)(maker-iter.1-setup.o)

maker-iter.1-run-all-resources-tasks := 1
maker-iter.1-run-all-resources-cpus  := 1 
maker-iter.1-run-all-resources-time  := 2:00:00
maker-iter.1-run-all.o: $(queued)(maker-iter.1-run.o)

#### busco 
sections += busco.o
busco-resources-tasks := 1
busco-resources-cpus  := $(max-cpus)
busco-resources-time  := 12:00:00
busco.o: $(queued)(maker-iter.1-run-all.o)

#### filter 
sections += filter.o
filter-resources-tasks := 1
filter-resources-cpus  := 1
filter-resources-time  := 3:00:00
filter.o: $(queued)(busco.o)

#### iprscan 
sections += iprscan-prepare.o iprscan-run.o iprscan-combine.o

iprscan-prepare-resources-tasks := 1
iprscan-prepare-resources-cpus  := 1
iprscan-prepare-resources-time  := 4:00:00
iprscan-prepare.o: $(queued)(filter.o)

iprscan-run-resources-tasks := 1
iprscan-run-resources-cpus  := $(max-cpus)
iprscan-run-resources-time  := $(max-time)
iprscan-run-resources-array := 1-$(MIKADO_SPLIT)
iprscan-run.o: $(queued)(iprscan-prepare.o)

iprscan-combine-resources-tasks := 1
iprscan-combine-resources-cpus  := 1
iprscan-combine-resources-time  := 4:00:00
iprscan-combine.o: $(queued)(iprscan-run.o)


#### blast 
sections += blast.o
blast-resources-tasks := 1
blast-resources-cpus  := $(max-cpus)
blast-resources-time  := $(max-time)
blast.o: $(queued)(filter.o)

#### rename
sections += rename.o
rename-resources-tasks := 1
rename-resources-cpus  := 1
rename-resources-time  := 4:00:00
rename.o: $(queued)(blast.o iprscan-combine.o)

#### final reports

sections += final-busco-transcript.o
final-busco-transcript-resources-tasks := 1
final-busco-transcript-resources-cpus  := $(max-cpus)
final-busco-transcript-resources-time  := $(max-time)
final-busco-transcript.o: $(queued)(rename.o)

sections += final.o
final-resources-tasks := 1
final-resources-cpus  := 1
final-resources-time  := 4:00:00
final.o: $(queued)(rename.o busco-genome.o)

#### busco-genome
sections += busco-genome.o
busco-genome-resources-tasks := 1
busco-genome-resources-cpus  := $(max-cpus)
busco-genome-resources-time  := $(max-time)
busco-genome.o: $(queued)(prepare.o)


all : $(queued)(rename.o busco-genome.o final-busco-transcript.o final.o)
#all : $(queued)(prepare.o)

# Requeue jobs that failed. FIXME: Didn't work with running array.
failed:
	sacct -X --format='JobName%100,State' -j $$(ar p $(queued) | tr '\n' ',') \
    | awk '$$2 == "FAILED" { print $$1 ".o" }' \
    | xargs --no-run-if-empty ar d $(queued)
	sacct -X --format='JobID%25,State' -j $$(ar p $(queued) | tr '\n' ',') \
    | awk '$$2 == "PENDING" { print $$1 }' \
    | xargs --no-run-if-empty scancel
	$(MAKE) $(MFLAGS) -f $(abspath $(lastword $(MAKEFILE_LIST))) all

# Show status of pipeline
show :
	sacct -X --format='Comment,JobName%30,JobID,State,Start,End' \
		-j $$(ar p $(queued) | tr '\n' ',')

# Does resources variable contain --array
is-array = $(findstring --array,$(call create_queue_resources,$1-resources))

# Set output name, omitting array id number if not array
output-name = $(if $(call is-array,$1),\
					"slurm-%A-%a.$1.out",\
					"slurm-%j.$1.out")

# Get the slurm job ids from the archive
# - $(shell $(AR) p) prints the saved sbatch output to stdout
# - $(strip ) removes leading/trailing whitespace
# - $(subst $(space),: ) replaces space with ':' for use in sbatch
dep-list = $(if $1,--dependency=afterok:$(subst $(space),:,$(strip \
			$(shell $(AR) p $(queued) $1))))

#Setup task with array
task = $1$(if $(call is-array,$1),-$$SLURM_ARRAY_TASK_ID)
# delete .o files after finished
.INTERMEDIATE: $(sections)
# export all variables given to make
.EXPORT_ALL_VARIABLES:
$(sections) : %.o :
#	$(MAKE) -f $(pipeline)/annotate.mf $(MFLAGS) -q $* &&
		$(queue_cmd) $(queue_extra) \
			$(call create_queue_resources,$*-resources) \
			$(call dep-list,$(strip $^)) \
			--job-name $* \
			--comment "$(NAME)" \
			--output $(call output-name,$*) \
			--error  $(call output-name,$*) \
			--export=ALL \
			--wrap '$(MAKE) $(MFLAGS) -j $$SLURM_NTASKS CPUS=$$SLURM_CPUS_PER_TASK \
					-f $(pipeline)/annotate.mf $(call task,$*)' > $@

## Default target. If target give that doesn't have a recipe, assume it's the
## name of a target and try to queue it.
%: $(queued)(%.)
