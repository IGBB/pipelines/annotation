pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

SHELL := /bin/bash
.EXPORT_ALL_VARIABLES:

SINGULARITY_CACHEDIR = $(shell mktemp -d)

all: tetools_1.5.sif trinity_2.11.0--h5ef6573_0.sif gffread-0.12.3 gmes_linux_64/ rlibs/ gmap-2020-10-14/bin/ seqtk/seqtk maker_3.01.03--pl5262h8f1cd36_1.sif busco_v5.2.2_cv1.sif

busco_v5.2.2_cv1.sif:
	ml singularity && \
	singularity pull $@.tmp.sif docker://ezlabgva/busco:v5.2.2_cv1
	mv $@.tmp.sif $@

seqtk/seqtk:
	cd $(pipeline)/seqtk && make

maker_3.01.03--pl5262h8f1cd36_1.sif:
	ml singularity && \
	singularity pull $@.tmp.sif docker://quay.io/biocontainers/maker:3.01.03--pl5262h8f1cd36_1
	mv $@.tmp.sif $

tetools_1.5.sif:
	ml singularity && \
	singularity pull $@.tmp.sif docker://dfam/tetools:1.5
	mv $@.tmp.sif $@

trinity_2.11.0--h5ef6573_0.sif:
	ml singularity && \
	singularity pull $@.tmp.sif docker://quay.io/biocontainers/trinity:2.11.0--h5ef6573_0
	mv $@.tmp.sif $@


gffread-0.12.3:
	wget -O- https://github.com/gpertea/gffread/releases/download/v0.12.3/gffread-0.12.3.Linux_x86_64.tar.gz | \
	tar -xz
	cp gffread-0.12.3.Linux_x86_64/gffread $@


gmes_linux_64/ rlibs/: %/ : %.tar.gz
	tar -xzvf $<

gmap-2020-10-14/bin/:
	wget -O-  http://research-pub.gene.com/gmap/src/gmap-gsnap-2020-10-14.tar.gz \
	| tar -xz
	cd gmap-2020-10-14; ./configure --prefix=`pwd` && make && make install


interproscan:
	$(MAKE) $(MFLAGS) \
	    -C $(pipeline) \
	    -f $(pipeline)/config-iprscan.mk \
	    download
