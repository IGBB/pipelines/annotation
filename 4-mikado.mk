# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(pipeline)/config.mk


#########################################
####### Required Input Parameters #######
#########################################
#                    NAME        Description
$(call var_required, GENOME,     Input Genome)
$(call var_required, LIBS,       RNA-Seq Input Library Names)
$(call var_required, ALIGNMENTS, RNA-Seq Library bam files)
$(call var_required, CPUS,       Number of threads to use for each task)
$(call var_required, SCORE,      Mikado scoring matrix [mammalian.yaml$(comma) plant.yaml])

.PHONY: all
all: subloci

.PHONY: not-trinity
not-trinity: stringtie cufflinks

################################################################################
## 1) Trinity
.PHONY: trinity trinity-align trinity-assemble
################################################################################
trinity: trinity-assemble trinity-align portcullis
trinity-assemble: trinity/Trinity-GG.fasta
trinity-align: trinity.gff

##### Merge all alignments
trinity/merged.bam: $(ALIGNMENTS) | trinity/
	$(call sexec,$^,$(samtools)) samtools merge -f $@.tmp.bam $^
	mv $@.tmp.bam $@

##### Run Trinity
# TODO: Add a mem option
trinity/Trinity-GG.fasta: trinity/merged.bam trinity/merged.bam.bai | trinity/
	$(call sexec,$^,$(trinity)) \
		Trinity --genome_guided_bam $< \
				--genome_guided_max_intron 100000 \
				--max_memory 100G \
				--CPU $(CPUS) \
				--output $|

##### Setup gmap database
# NOTE: gmap creates a directory for the database; so this isn't the safest
# setup. But it works from now
trinity/gmap.db: $(GENOME) | trinity/
	PATH=$$PATH:$(pipeline)/gmap-2020-10-14/bin/ \
	gmap_build -D $(dir $@) -d $(notdir $@) $<

##### Map trinity transcripts to genome using gmap
trinity.gff : trinity/Trinity-GG.fasta \
				trinity/gmap.db \
			|	trinity/
	PATH=$$PATH:$(pipeline)/gmap-2020-10-14/bin/ \
	gmap -D $(dir $(word 2,$^)) -d $(notdir $(word 2,$^)) \
		 -t $(CPUS) -f gff3_gene $< > $@.tmp
	mv $@.tmp $@

################################################################################
## 2) Stringtie
.PHONY: stringtie
## TODO: Figure out a safer way to match alignment to stringtie. patsubst maybe
################################################################################
stringtie: stringtie.gtf

# Get directory of the first alignment
ALIGN_DIR := $(dir $(word 1, $(ALIGNMENTS)))
STRINGTIE := $(addprefix stringtie/, $(addsuffix .gtf, $(LIBS)))

##### Run strigtie on each alignment file
# Split so `make -j` can deal with multithreading
$(STRINGTIE) : stringtie/%.gtf : $(ALIGN_DIR)/%.bam \
			 					| stringtie/
	$(call sexec,$^,$(stringtie)) stringtie $< -o $@.tmp
	mv $@.tmp $@

##### Merge library stringtie gtf's into single gtf
stringtie.gtf : $(STRINGTIE)
	$(call sexec,$^,$(stringtie)) stringtie --merge -o $@.tmp $^
	mv $@.tmp $@

################################################################################
## 3) Cufflinks
.PHONY: cufflinks
################################################################################
cufflinks: cufflinks.gtf

##### Run cufflinks
# Move output to top directory
cufflinks.gtf: $(ALIGNMENTS) | cufflinks/
	$(call sexec,$^,$(cufflinks)) \
		cufflinks $^ -p $(CPUS) -o $| --no-update-check
	mv cufflinks/transcripts.gtf $@


################################################################################
## 4) Portcullis
.PHONY: portcullis
################################################################################
portcullis: portcullis.bed

##### Run portcullis
# Move final output to top directory
portcullis.bed : $(GENOME) trinity/merged.bam | portcullis/
	$(call sexec,$^,$(portcullis)) \
		portcullis full -t $(CPUS) --strandedness unstranded -o $| $^
	mv $|/3-filt/portcullis_filtered.pass.junctions.bed $@

################################################################################
## 5) Mikado Config and Prepare
.PHONY : setup config prepare
################################################################################
setup: list.txt
config: config.yaml
prepare: .prepare mikado.split.mk

###### Create list of annotations for mikado
list.txt:  trinity.gff stringtie.gtf cufflinks.gtf
	printf "%s\tTrinity\tFalse\t-0.5\n" $(abspath $(word 1,$^))  > $@.tmp
	printf "%s\tStringtie\tFalse\t1\n"  $(abspath $(word 2,$^)) >> $@.tmp
	printf "%s\tCufflinks\tFalse\n"     $(abspath $(word 3,$^)) >> $@.tmp
	mv $@.tmp $@

##### Configure mikado
config.yaml: $(GENOME) list.txt portcullis.bed $(UNIPROT)
	$(call sexec,$^,$(mikado)) \
		mikado configure --scoring       $(SCORE) \
						 --reference     $(abspath $(word 1,$^))    \
						 --list          $(abspath $(word 2,$^))    \
						 --junctions     $(abspath $(word 3,$^))    \
						 --blast_targets $(abspath $(word 4,$^))    \
						 $@.tmp.yaml
	mv $@.tmp.yaml $@


##### Prepare fasta
# mikado prepare creates the output file immediately. So we need an intermediate
# file to flage if process completed successfully.
mikado_prepared.fasta : .prepare
.prepare : config.yaml $(GENOME) $(UNIPROT)
	$(call sexec,$^,$(mikado)) \
		mikado prepare -p $(CPUS) --json-conf $(abspath $<)
	touch $@

##### Split prepared fasta and create stored variable
# The serialise step run quicker if the blast xml files are split into chunks.
# The easiest way to do that is to split the fasta. This also adds the ability
# to use job arrays.
#    split_fasta.py
#         Included in mikado. NOTE: DO NOT use on genomes.
#                                   It takes forever on large sequences.
#    realpath -e --relative-to .
#         get path realtive to current directory (--relative-to) to fasta files,
#         ensuring files exist (-e)
mikado.split.mk: mikado_prepared.fasta
	$(call sexec,$^,$(mikado)) split_fasta.py -m $(SPLIT) $< $(basename $<)
	ls $(basename $<)_*.fasta \
	| xargs realpath -e --relative-to . \
	| xargs printf 'MIKADO_FASTAS += %s\n' > $@.tmp
	mv $@.tmp $@

##### Inlcude stored MIKADO_FASTAS variable
# TODO: check on making this variable on the fly.
#      Since the mikado.split.mk file is included, the entire mikado process
#      tries to run if the file doesn't exist, even with `make -n`.
#-include mikado.split.mk
MIKADO_FASTAS := $(wildcard mikado_prepared_*.fasta)

################################################################################
## 6) Blast transcripts
.PHONY : blast
## Job Array Targets : blast-run-#
################################################################################
MIKADO_BLASTS := $(subst fasta,blast.xml.gz,$(MIKADO_FASTAS))
blast: $(MIKADO_BLASTS)
$(call job-array-targets,blast,$(MIKADO_BLASTS))


##### Blast transcripts against uniprot
$(MIKADO_BLASTS): %.blast.xml.gz: %.fasta $(UNIPROT)
	$(call sexec,$^,$(blast)) \
	blastx  -max_target_seqs 5 \
			-num_threads $(CPUS) \
			-query $< \
			-outfmt 5 \
			-db $(UNIPROT) \
			-evalue 0.000001 2> $@.log \
	| sed '/^$$/d' \
	| gzip -c - > $@.tmp
	mv $@.tmp $@


################################################################################
## 7) ORF Finding
.PHONY : prodigal
##   Latest version of transdecoder is having problems playing nice with mikado.
##   Switching to prodigal as suggested by mikado developers
################################################################################
prodigal: prodigal.gff3

# 4-mikado/$(NAME)/transdecoder.bed: 4-mikado/$(NAME)/mikado_prepared.fasta \
# 								 | 4-mikado/$(NAME)/transdecoder/
# 	ml singularity && \
# 	singularity exec $(transdecoder) TransDecoder.LongOrfs -t $< -O $| && \
# 	singularity exec $(transdecoder) TransDecoder.Predict -t $< -O $| --single_best_only
# 	# mv $(notdir $<).transdecoder.{bed,cds,gff3,pep} $|
# 	# mv $(notdir $<).transdecoder.bed $@

##### Run Prodigal
prodigal.gff3: mikado_prepared.fasta
	ml prodigal && prodigal -g 1 -f gff -i $< -o $@.tmp
	mv $@.tmp $@

################################################################################
## 8) Mikado Serialise and Pick
.PHONY : serialise subloci
################################################################################
serialise: .serialise
subloci: subloci.gff3

##### Add data to mikado
# FIXME: Creates a db file needs to be remove is run fails.
.serialise : config.yaml $(UNIPROT) prodigal.gff3 mikado_prepared.fasta \
			 $(MIKADO_BLASTS) $(GENOME)
	$(call sexec,$^,$(mikado)) \
		mikado serialise 	--json-conf     $(word 1, $^) \
							--blast_targets $(word 2, $^) \
							--orf           $(word 3, $^) \
							--transcripts   $(word 4, $^) \
							--xml           $(subst $(space),$(comma),$(MIKADO_BLASTS)) \
							-p $(CPUS)
	touch $@

##### Call final transcripts
subloci.gff3: config.yaml .serialise $(GENOME)
	$(call sexec,$^,$(mikado)) \
		mikado pick --json-conf $< --subloci-out $@.tmp.gff3 -p $(CPUS)
	mv $@.tmp.gff3 $@
