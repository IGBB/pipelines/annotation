#### Global Config Variables and Rules

# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

#### Include GNU make standard library
include $(pipeline)/gmsl/gmsl

# Create variables of constants make has a hard time with
empty :=
comma := ,
space := $(empty) $(empty)

host := $(shell hostname)

include $(pipeline)/config-singularity.mk
include $(pipeline)/config-iprscan.mk

## BUSCO call (needed because of dataset setup)
## There's a big difference between machine setups to run busco
## Need to check which machine we're running on

busco_container:=$(pipeline)/busco_v5.2.2_cv1.sif
# busco_overlay:=/work/datasets/igbb/busco4/datasets.20201008.img
# ifneq ("$(empty)","$(wildcard $(busco_overlay))")
# 	busco = $(singularity) \
# 		--overlay $(busco_overaly) \
# 		-B /work/datasets/igbb/busco4/config.ini:/busco/config/config.ini \
# 		$(call container_binds,$1) \
# 		$2 \
# 		$(busco_container) busco
# else ifneq ("$(empty)","$(wildcard /reference/data/BUSCO/v4/)")
	busco = $(singularity) \
		-B $(pipeline)/busco.config.ini:/busco/config/config.ini \
		$(call container_binds,$1) \
		$2 \
		$(busco_container) busco
# else
# 	busco = $(error "Can't find busco datasets")
# endif

################################################################################
## General Rules
################################################################################

# Automatically create sub-directories
%/:
	mkdir -p $@

# Index bam files
%.bai: %
	$(call sexec,$<,$(samtools)) samtools index $<
# Index fasta files
%.fai: %
	$(call sexec,$<,$(samtools)) samtools faidx $<


################################################################################
## Validate Input Variables
################################################################################

# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
var_required = \
	$(strip $(foreach 1,$1, \
		$(call __var_required,$1,$(strip $(value 2)))))
__var_required = \
	$(if $(value $1),, \
		$(error Undefined $1$(if $2, ($2))))

################################################################################
### JOB ARRAY TARGETS
### Creates phony numbered targets named "$(prefix)-run-$(#)" for a given list
###	REQUIRES:
###	    1) prefix of phony target
###	    2) list of files
### USAGE :
###     $(call job-array-targets,markdups,$(MARKDUPS))
###
################################################################################
define __job-array-targets
.PHONY: $(strip $1)-run-$(strip $2)
$(strip $1)-run-$(strip $2): $(word $2, $3)
endef
job-array-targets = $(foreach num,$(shell seq 1 $(words $2)), \
						$(eval $(call __job-array-targets,$(strip $1),$(num),$2)))
