# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(pipeline)/config.mk

#########################################
####### Required Input Parameters #######
#########################################
#                    NAME      Description
$(call var_required, FASTA,   Input Genome)
$(call var_required, TYPE,     RNA-Seq Input Library Names)
$(call var_required, LINEAGE,  Busco lineage database name )
$(call var_required, CPUS,     Number of threads to use for each task)

.PHONY: all images


all: busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt

# Must have sentienal file or it conflicts with %/ rule
# augustus-species/.complete:
# 	$(singularity) $(busco_container) cp -r /augustus/config/species $(dir $@)
# 	touch $@

busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt: $(FASTA) $(LINEAGE)
	$(call busco,$^,) -i $< \
		-l $(LINEAGE) \
		-c $(CPUS) \
		-m $(TYPE) \
		-o busco \
		--offline

busco/run_$(LINEAGE)/full_table.tsv:  busco/short_summary.specific.$(notdir $(LINEAGE)).busco.txt

images: busco/run_$(notdir $(LINEAGE))/full_table.tsv $(ANNO)
	ml r/4.0.2 && \
	Rscript $(pipeline)/images.R --annotation $(ANNO) \
								 --busco $< \
								 --out $(NAME)
	perl $(pipeline)/busco-aed.pl $^ > aed.table.txt
