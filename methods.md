# General Methods

    
RepeatModeler [v2.0.1] was used to create a species specific database of
repeats, including LTR structural elements, for *SPECIES*. RepeatMasker [v4.1.1]
was used to annotate and softmask the repeats from the de-novo database
in the genome. RNA-seq datasets (*SRA accession**) were mapped to the genome using
hisat2 [v2.1.0]. BRAKER2 [v2.1.5], using GeneMark-ES [v4.61], the softmasked
genome, and the RNA-seq alignments, was used to train Augustus [v3.3.3] and SNAP
[v2013-02-16]. The Mikado [v2.0] pipeline was used to predict transcripts based
on the RNA-seq alignments. Breifly, the trascripts predicted by Trinity
[v2.11.0] mapped with GMAP [v2020-10-14], Cufflinks [v2.2.1], and Stringtie
[v2.1.2] were combined into a non-redundant set and mapped to the
Uniprot-Swissport database [v2020-05] using BLAST+ [v2.9.0]. Additionally,
Prodigal [v2.6.3] was used to find ORFs with the transcripts, and Portcullis
[v1.2.2] was used to predict splice sites from the read alignments. Finally,
Mikado used the splice site predictions, the predicted transctipts, and the
protien alignments to identify gene loci and the representative transcript for
each. Maker [v2.31.10] was used with the RepeatModeler/RepeatMasker annotations;
the SNAP, Augustus, GeneMark, and Mikado ab-inition predictions; the protein
evidence from previously annotated genomes G. hirstum and G. raimondii, along
with the Uniprot Swissprot database [v2020-05]; and all available est from NCBI
for the Gossypium genus (downloaded 2020-09-03, search filter:
txid3633[Organism:exp] AND is_est[filter]) used as alternative EST evidence to
predict structural annotations. BUSCO [v4.1.2] was used with the eudicot [odb10]
database against the predicted transctripts to find a satisfactory AED filter.
The filtered annotations were functionally annotated using InterProScan
[v5.47-82.0] and BLAST+ with the Uniprot Swissprot database.
