################################################################################
## Makefile containing the config and setup for iprscan
################################################################################

# Path to pipeline makefiles
pipeline := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(pipeline)/config-singularity.mk



################################################################################
## INTERPRO CONFIG
## 	- IPR.FTP :: FTP url for iprscan
## 	- IPR.DKR :: Dockerhub url for iprscan
## 	- IPR.VER :: Version of iprscan do download and use
## 	- interproscan :: run command
################################################################################
IPR.FTP := ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan
IPR.DKR := docker://interpro/interproscan
IPR.VER := 5.51-85.0

interproscan:= $(singularity) \
      -B $(pipeline)/interproscan-$(IPR.VER)/data:/opt/interproscan/data \
      $(call container_binds,$1) \
      $(pipeline)/interproscan_$(IPR.VER).sif interproscan.sh



################################################################################
## INTERPRO DOWNLOADS
################################################################################

download: interproscan-$(IPR.VER)/.complete

interproscan_$(IPR.VER).sif:
	ml singularity && \
	singularity pull $(IPR.DKR):$(IPR.VER)

interproscan-data-$(IPR.VER).tar.gz interproscan-data-$(IPR.VER).tar.gz.md5:
	wget $(IPR.FTP)/5/$(IPR.VER)/alt/$@

interproscan-$(IPR.VER)/data/.download: interproscan-data-$(IPR.VER).tar.gz \
                                        interproscan-data-$(IPR.VER).tar.gz.md5
	md5sum -c $(word 2, $^)
	tar -pxzf $<
	touch $@

interproscan-$(IPR.VER)/.complete: test_all_appl.tsv
	touch $@

.INTERMEDIATE: test_all_appl.txt
test_all_appl.tsv: interproscan_$(IPR.VER).sif \
                        interproscan-$(IPR.VER)/data/.download
	$(call interproscan,) \
	    --input /opt/interproscan/test_all_appl.fasta \
	    --disable-precalc \
	    --formats  tsv \
	    --outfile $@ \
	    --tempdir /tmp
