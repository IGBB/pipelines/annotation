#!/usr/bin/env perl

use strict;
use warnings;
use autodie;
use sort 'stable';

use Getopt::Long;
use File::Spec::Functions qw/catfile/;
use File::Basename;
use List::Util qw/first/;

my $arg = {};
GetOptions($arg,
           "prefix=s",
           "number=i",
           "round-robin");

my $input = shift || '/dev/stdin';

my $base = $arg->{prefix} || basename($input, qw/.fa .fasta/);
my $outfiles = [map {open(my $fh, '>', sprintf("%s_%03d.fasta", $base, $_)); $fh}
                (1..$arg->{"number"}) ];

open(my $fh, $input);

# file index
# - Start at -1 to make the round robin code nicer
my $idx = -1;

my $tell = {map {$_ => 0} (0..($arg->{"number"}-1))};

my $out = undef;
while(<$fh>){
    if(/^>/){
        if($arg->{"round-robin"}){
            $idx = ($idx+1) % $arg->{"number"};
        }else{
            # Get the file with the fewest bytes written
            ## - Stable sort should ensure the first element is the lowest file
            ##   number with fewest bytes written
            $idx = first {defined($_)}
                        sort {$tell->{$a} <=> $tell->{$b}}
                        (0..($arg->{"number"}-1));
        }
        $out = $outfiles->[$idx];
    }

    print {$out} $_;
    $tell->{$idx} = tell($out);
}

### TODO: Delete empty files so they don't get picked up by the makefile
